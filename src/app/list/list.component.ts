import { Component, OnInit } from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import {AuthHttp} from 'angular2-jwt';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  baseUrl: 'https://graph.microsoft.com/v1.0/sites//99xtech.sharepoint.com,691d466f-cca8-4343-976c-979534993b2e,5e67b789-4664-4ab5-beef-24abf3f9568d/lists';
  tokenString: String;
  list: any [];

  constructor(private authHttp: AuthHttp) { }

  ngOnInit() {


    this.authHttp.get('https://graph.microsoft.com/v1.0/sites//99xtech.sharepoint.com,691d466f-cca8-4343-976c-979534993b2e,5e67b789-4664-4ab5-beef-24abf3f9568d/lists').subscribe(response => {
      const user = response.json();
     this.list = user.value;
     console.log(this.list);


    });


  }

  private requestOptions() {
    const headers = new Headers({
      'Content-type': 'application/json',
      'Authorization': 'Bearer ' + this.tokenString
    });
    return new RequestOptions({headers: headers});
  }
}
