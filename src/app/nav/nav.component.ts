import { Component, OnInit } from '@angular/core';
import {JwtHelper, tokenNotExpired} from 'angular2-jwt';
import {Router} from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  userToken: any;
  userName: any;
  jwtHelper: JwtHelper = new JwtHelper();
  constructor(private router: Router) { }

  ngOnInit() {
  }

  loggedIn() {

    return tokenNotExpired('token');
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/']);
  }

}
