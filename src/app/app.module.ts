import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import {HttpModule} from '@angular/http';
import { CallbackComponent } from './callback/callback.component';
import {RouterModule} from '@angular/router';
import {appRoutes} from '../routes';
import { HomeComponent } from './home/home.component';
import {AuthService} from './auth.service';
import {AuthGuard} from './_guard/auth.guard';
import { ListComponent } from './list/list.component';
import {AuthModule} from './auth/auth.module';
import { ListItemsComponent } from './list-items/list-items.component';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    CallbackComponent,
    HomeComponent,
    ListComponent,
    ListItemsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    AuthModule
  ],
  providers: [
    AuthService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
