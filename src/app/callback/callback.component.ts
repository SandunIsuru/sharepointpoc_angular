import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {JwtHelper} from 'angular2-jwt';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.css']
})
export class CallbackComponent implements OnInit {

  baseUrl = 'https://login.microsoftonline.com/common/oauth2/v2.0/token';
  clientId = '2ae1871d-7e2d-4961-b780-762d544cb753';
  client_secret = 'qzndGGEI4636_~_vwsRTV9~';
  redirect_uri = 'http://localhost:4200/callback';
  scope = 'User.Read Tasks.ReadWrite.Shared Tasks.ReadWrite Tasks.Read.Shared Tasks.Read Sites.ReadWrite.All Sites.Read.All';
  userToken: any;
  decodedToken: any;
  jwtHelper: JwtHelper = new JwtHelper();

  constructor(private activeRoute: ActivatedRoute, private http: Http, private router: Router) { }

  ngOnInit() {

    const code = this.activeRoute.snapshot.queryParams.code;
    const routeParams = this.activeRoute.snapshot.params;


    let data = 'grant_type=authorization_code&client_id=' + this.clientId + '&code=' + code + '&redirect_uri=' + this.redirect_uri + '&client_secret=' + this.client_secret + '&scope=' + this.scope;

      this.http.post(this.baseUrl, data, this.requestOptions()).subscribe(response => {
        const user = response.json();

        if (user.access_token) {
          this.router.navigate(['/home']);
          localStorage.setItem('token', user.access_token);
          this.decodedToken = this.jwtHelper.decodeToken(user.access_token);
          console.log(this.decodedToken);
        } else {
          console.log('error');
        }
    });

  }




  private requestOptions() {
    const headers = new Headers({
      'Content-type': 'application/x-www-form-urlencoded'
    });
    return new RequestOptions({headers: headers});
  }

  private handleError(error: any) {
    const applicationError = error.headers.get('Application-Error');
    if (applicationError) {
      return Observable.throw(applicationError);
    }
    const serverError = error.json();
    let modelStateErrors = '';
    if (serverError) {
      for (const key in serverError) {
        if (serverError[key]) {
          modelStateErrors += serverError[key] + '\n';
        }
      }
    }
    return Observable.throw(
      modelStateErrors || 'Server error'
    );
  }

}
