import { Injectable } from '@angular/core';
import {tokenNotExpired} from 'angular2-jwt';

@Injectable()
export class AuthService {

  userToken: any;
  decodedToken: any;

  constructor() { }
  loggedIn() {
    return tokenNotExpired('token');
  }
}
