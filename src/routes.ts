import {Routes} from '@angular/router';
import {CallbackComponent} from './app/callback/callback.component';
import {AuthGuard} from './app/_guard/auth.guard';
import {HomeComponent} from './app/home/home.component';
import {ListItemsComponent} from './app/list-items/list-items.component';


export const appRoutes: Routes = [

  {path: 'callback', component: CallbackComponent},
  {path: 'home', component: HomeComponent},
  {path: 'list_items/:id', component: ListItemsComponent}

];

